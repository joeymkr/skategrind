/**
 * Created by Venture on 13/05/2015.
 */

function validateContactForm(){
    var formIsValid = true;
    var inputDetails = {};

    inputDetails.phone = $('#phone').val().trim();
    inputDetails.address = $('#addrLine1').val().trim();
    inputDetails.town = $('#town').val().trim();
    inputDetails.county = $('#county').val().trim();
    inputDetails.postcode = $('#postcode').val().trim();
    inputDetails.price = $('#price').val().trim();

    for(var entry in inputDetails) {
        $('#' + entry + 'warning').text('');
    }

    for (var entry in inputDetails) {
        if (inputDetails[entry] === ""){
            $('#' + entry + 'warning').text('*');
            formIsValid = false;
        }
    }

    if (!$.isNumeric(inputDetails.price)){
        formIsValid = false;
    }

    return formIsValid;
}

function validateOpenTimesForm(){
    var formIsValid = true;
    var inputDetails = {};

    inputDetails.push($('#Mondayopen').val().trim());
    inputDetails.push($('#Mondayclose').val().trim());
    inputDetails.push($('#Tuesdayopen').val().trim());
    inputDetails.push($('#Tuesdayclose').val().trim());
    inputDetails.push($('#Wednesdayopen').val().trim());
    inputDetails.push($('#Wednesdayclose').val().trim());
    inputDetails.push($('#Thursdayopen').val().trim());
    inputDetails.push($('#Thursdayclose').val().trim());
    inputDetails.push($('#Fridayopen').val().trim());
    inputDetails.push($('#Fridayclose').val().trim());
    inputDetails.push($('#Saturdayopen').val().trim());
    inputDetails.push($('#Saturdayclose').val().trim());
    inputDetails.push($('#Sundayopen').val().trim());
    inputDetails.push($('#Sundayclose').val().trim());

    var days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

    for (var dayEntry in days){
        $('#' + dayEntry + 'warning').text('');
    }

    for (var i = 0; i < inputDetails.length; i+=2){
        formIsValid = validateTimes(parseFloat(inputDetails[i]),parseFloat(inputDetails[i+1]));
        if (!formIsValid){
            $('#' + days[i/2] + 'warning').text('*');
        }
    }

    return formIsValid;
}

function validateTimes(open, close){
    if (open >= close){
        return false;
    }

    return true;
}

function validatePostForm(){
    var formIsValid = true;
    var inputDetails = {};

    inputDetails.title = $('#posttitle').val().trim();
    inputDetails.post = $('#postbody').val().trim();
    inputDetails.image = $('#imageupload').val().trim();

    for(var entry in inputDetails) {
        $('#' + entry + 'warning').text('');
    }

    if (inputDetails.title === ""){
        $('#' + entry + 'warning').text('*');
        formIsValid = false;
    }

    if (inputDetails.post === ""){
        $('#' + entry + 'warning').text('*');
        formIsValid = false;
    }

    return formIsValid;
}
