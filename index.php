<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>
		SkateGrind
	</title>
	<link rel="stylesheet" type="text/css" href="css/skate-grind.css"/>

	<script src="js/jquery/jquery.min.js"></script>

</head>

<body>
<div id="container">

	<?php include('includes/header.php');?>

	<?php include('includes/content.php');?>

	<?php include('includes/footer.php');?>

</div>

</body>
</html>