<?php include('includes/admin/dbConnect.php');
$connection = getConnection();

$db = mysqli_select_db($connection, "skategrind") or die("cannot select db");

$query = mysqli_query($connection, "SELECT * FROM info;");

$infoData = mysqli_fetch_assoc($query);

$query = mysqli_query($connection, "SELECT * FROM opening_times WHERE start_time IS NOT NULL AND end_time IS NOT NULL;");

$openingtimes = "";

while ($openingRow = mysqli_fetch_assoc($query)){
    $openingtimes .= "<p>".$openingRow['day'].": ".$openingRow['start_time']." - ".$openingRow['end_time']."</p>";
}

$welcome = 'Welcome to SkateGrind';

$companyGoal = 'The SkateGrind vision is to exceed customers expectation by providing them with a fast and reliable sharpening service using top of the line products.';

$welcomeinfo = 'We cater for all types of ice skating and guarantee, our grinds will give you the best performance be it figure skating, hockey or recreational use. SkateGrind also offers skate maintenance, blade alignment and professional advice to get the most out of your ice time.';

$opentitle = 'Opening Times';

$contact = 'Contact Us';

$telephone = $infoData['phone'];

$email = urldecode($infoData['email']);

$address_line_1 = $infoData['address_line_1'];
$address_line_2 = $infoData['address_line_2'];
$town = $infoData['town'];
$county = $infoData['county'];
$postcode = $infoData['postcode'];

$address = '<p>'.$address_line_1.'</p>'.
'<p>'.$address_line_2.'</p>'.
'<p>'.$town.'</p>'.
'<p>'.$county.'</p>'.
'<p>'.$postcode.'</p>';

$prices = $infoData['price'];

$note = 'NOTE: we do not cater for the FLAT-BOTTOM V grind';


$whatWeDotitle = 'What We Do';

$whatWeDoPara = 'Skate Grind offers skate sharpening services to skaters of all ranges. We are based at Lee Valley Ice Centre.';

$whenToSharpen = 'Whether you are a figure skater, ice hockey player or a recreational skater, it is important to have sharp blades to skate smoothly. As a rule of thumb we recommend that skates are sharpened after every 20 hours of skating. If you are playing high-impact ice hockey we recommend skates to be be sharpened more regularly, usually every 8 hours of game time. ';

$generaltitle ='General Information';

$generalPara ='Your blade can be your best friend or your worst enemy. Whenever you skate, you trust your personal safety to it. It is the origin of every force that drives every movement that you make. If you intend to be a good skater, you need to know it like an extension of your body; how it feels and how to use it. You need for it to work the same all the time.';

$generalPara2 = 'We use a variety of wheel sizes to suit all types of skating preference and style. The most common question asked is what grind do I need, this is very individual matter, your hollow depth depends on many different factors such as skating style (figure, dance, hockey etc), body weight and skating preferences. We are here to offer professional advice to all, below you will find infomation on blade depths, skate maintenance and other skating information.';

$generalinfotitle ='The hollow';

$generalBladeInfo = 'A skate blade is sharpened by creating a perfectly rounded valley, centered on the bottom of the blade. This forms a pair of sharp edges at the outer extremities of the blades giving skaters control on the ice. This valley is the hollow.';

$generalBladeInfoHollow = 'The radius of hollow can be varied from &frac14;-inch to 1 &frac12;-inch, however most skaters will choose a hollow between 3/8-inch and 5/8-inch. The depth of hollow changes the feel of the skates on the ice. A deep hollow such as 3/8-inch will give significant &quot;bite&quot; on the edges at the cost of glide. A shallower hollow like 5/8-inch will allow better glide with some loss of edge bite. So, if you&#8217;re tripping over your edges and landing on your head, the hollow you are using is likely too deep. If your skates are skidding wildly out of control, you probably need to go deeper on your hollow.';

$facebookLink = urldecode($infoData['facebook']);

$linkedinLink = urldecode($infoData['linkedin']);

$twitterLink = urldecode($infoData['twitter']);

mysqli_close($connection);
?>