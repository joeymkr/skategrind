---Welcome to Skategrind---

Mr Chungs Skategrind website and content management system
 
Date: 15/05/2015 
Author: Joe Roberts 
Owner: Joe Roberts
client: Jon Chung
URL: http://www.skategrind.org.uk/

Website runs on PHP and MySQL and allows content management for admin users to the site. 
Changes can be made to contact information including phone, email, address, prices, 
opening times and social networking site links.

Supplied is a blog feed which allows dated posts and image file uploads. This file upload 
only permits image type files and nothing greater then 500000kb in size.



**********************To Run*********************

To run the website place the skategrind dir in the server root folder. Then call -

	server_url/skategrind/

To access the admin section call -

	server _url/skategrind/login



***************Database Dependencies**************

Supplied is a exported .sql file to be used to set up the MySQL db. Create a db called 
skategrind and import db/skategrind.sql. 

if problems occur with database connection on the site check /includes/admin/dbConnect.php 
and correct the server name, username and password in line 4.


NOTE: version 1.1