<script type="text/javascript">
	$(function(){
		$('#update').hide().fadeIn(1000);
	});
</script>

<div id="update">

	<h1><?php echo $contact ?></h1>
	<pre><?php echo $openingtimes ?></pre>
	<pre><?php echo $address ?></pre>
	<p>Phone: <?php echo $telephone ?></p>
	<p>Email: <?php echo $email ?></p>
</div>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script><div style="margin-top: 15px;margin-right: 10px;float:right;overflow:hidden;height:350px;width:500px;"><div id="gmap_canvas" style="height:350px;width:500px;"></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style><a class="google-map-code" href="http://www.map-embed.com" id="get-map-data">http://www.map-embed.com</a></div><script type="text/javascript"> function init_map(){var myOptions = {zoom:16,center:new google.maps.LatLng(51.563427, -0.044795),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(51.563427, -0.044795)});infowindow = new google.maps.InfoWindow({content:"<b>SkateGrind</b><br/>Lee Valley Ice Centre, Lea Bridge Rd<br/> E10 7QL London" });google.maps.event.addListener(marker, "click", function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>