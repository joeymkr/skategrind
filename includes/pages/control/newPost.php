<?php include("includes/admin/addPost.php");

?>

<script src="js/Validator.js"></script>
<div id="addPostForm">
    <form action="" method="POST" onsubmit="return validatePostForm();" enctype="multipart/form-data">
        <h1>Article</h1>
        <p><label class="field">Title </label></p>
        <p><input id="posttitle" name="posttitle" placeholder="Title" type="text" style="height:30px;float:right;"/><span id="posttitlewarning"></span></p>
        <p><label class="field">Post </label></p>
        <p><textarea id="postbody" name="postbody" placeholder="Type your post here"></textarea><span id="postbodywarning"></span></p>
        <p><label class="field">Image </label></p>
        <p><input id="imageupload" name="imageupload" placeholder="file url" type="file"/><span id="imageuploadwarning"></span></p>
        <p><input name="submit" type="submit" value="Post"></p>
    </form>
    <h2><span><?php echo$error; ?></span></h2>
</div>