<?php include('includes/admin/updateTimes.php');
?>
<script src="js/Validator.js"></script>
<div id="timesEditForm">
    <form action="" method="POST" onsubmit="return validateOpenTimesForm();">
    <?php
        $day = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
        $openingDays = getOpenDays();

        for ($i = 0; $i < 7; $i++){
            $openSelection = 'closed';
            $closeSelection = 'closed';
            if($openingDays != null){
                for ($n = 0; $n < sizeof($openingDays); $n++){
                    if ($openingDays[$n]['day'] == $day[$i]){
                        $openSelection = $openingDays[$n]['start'];
                        $closeSelection = $openingDays[$n]['end'];
                    }
                }
            }
        echo "<p><label class='field'>".$day[$i].": </label>".
            "<select id='".$day[$i]."open' name='".$day[$i]."open'>".
                "<option value='".$openSelection."'>".$openSelection."</option>".
                "<option value='09:00'>09:00</option>".
                "<option value='10:00'>10:00</option>".
                "<option value='11:00'>11:00</option>".
                "<option value='12:00'>12:00</option>".
                "<option value='13:00'>13:00</option>".
                "<option value='14:00'>14:00</option>".
                "<option value='15:00'>15:00</option>".
                "<option value='16:00'>16:00</option>".
                "<option value='17:00'>17:00</option>".
                "<option value='18:00'>18:00</option>".
                "<option value='19:00'>19:00</option>".
                "<option value='20:00'>20:00</option>".
                "<option value='21:00'>21:00</option>".
                "<option value='22:00'>22:00</option>".
                "<option value='closed'>closed</option>".
            "</select>".
            "-".
            "<select id='".$day[$i]."close' name='".$day[$i]."close'>".
                "<option value='".$closeSelection."'>".$closeSelection."</option>".
                "<option value='09:00'>09:00</option>".
                "<option value='10:00'>10:00</option>".
                "<option value='11:00'>11:00</option>".
                "<option value='12:00'>12:00</option>".
                "<option value='13:00'>13:00</option>".
                "<option value='14:00'>14:00</option>".
                "<option value='15:00'>15:00</option>".
                "<option value='16:00'>16:00</option>".
                "<option value='17:00'>17:00</option>".
                "<option value='18:00'>18:00</option>".
                "<option value='19:00'>19:00</option>".
                "<option value='20:00'>20:00</option>".
                "<option value='21:00'>21:00</option>".
                "<option value='22:00'>22:00</option>".
                "<option value='closed'>closed</option>".
            "</select><span id='".$day[$i]."warning'></span></p>";
        }
    ?>
        <input name="submit" type="submit" value="Save">
    </form>
    <h2><span><?php echo$error; ?></span></h2>
</div>