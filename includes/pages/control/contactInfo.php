<?php include('includes/admin/updateInfo.php');
?>
<script src="js/Validator.js"></script>
<div id="contactEditForm">
    <form action="" method="POST" onsubmit="return validateContactForm();">
        <label>Information</label>
        <p><label class="field">Telephone: </label><input id="phone" name="phone" value=<?php echo $telephone; ?> type="text"/><span id="phonewarning"></span></p>
        <p><label class="field">Email: </label><input id="email" name="email" value=<?php echo $email; ?> type="text"/></p>
        <p><label class="field">Facebook: </label><input id="facebook" name="facebook" <?php echo "value='".$facebookLink."'"; ?> type="text"/></p>
        <p><label class="field">LinkedIn: </label><input id="linkedin" name="linkedin" <?php echo "value='".$linkedinLink."'"; ?> type="text"/></p>
        <p><label class="field">Twitter: </label><input id="twitter" name="twitter" <?php echo "value='".$twitterLink."'"; ?> type="text"/></p>
        <p><label class="field">Address: </label><input id="addrLine1" name="addrLine1" <?php echo "value='".$address_line_1."'"; ?> type="text"/><span id="addresswarning"></span></p>
        <p><label class="field">Address: </label><input id="addrLine2" name="addrLine2" <?php echo "value='".$address_line_2."'"; ?> type="text"/></p>
        <p><label class="field">Town: </label><input id="town" name="town" <?php echo "value='".$town."'"; ?> type="text"/><span id="townwarning"></span></p>
        <p><label class="field">County: </label><input id="county" name="county" <?php echo "value='".$county."'"; ?> type="text"/><span id="countywarning"></span></p>
        <p><label class="field">Postcode: </label><input id="postcode" name="postcode" <?php echo "value='".$postcode."'"; ?> type="text"/><span id="postcodewarning"></span></p>
        <p><label class="field">Price Per Pair: </label><input id="price" name="price" value=<?php echo $prices; ?> type="text"/></p>
        <p><input name="submit" type="submit" value="Save"></p>
    </form>
    <h2><span><?php echo$error; ?></span></h2>
</div>