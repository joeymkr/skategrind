<script type="text/javascript">
	$(function(){
		$('#info').hide().fadeIn(1000);
		$('#update').hide().slideDown(1000);
		$('#block-image').hide().fadeIn(2000);
	});
</script>
<script type="text/javascript">

	$(function(){
		$('#block-image img:gt(0)').hide();
		setInterval(function(){
			$('#block-image :first-child').fadeOut(2000)
			.next('img').fadeIn(2000)
			.end().appendTo('#block-image');}, 6000);
	});
</script>
<div id="info">
		<h1><?php echo $welcome ?></h1>
		<p><?php echo $companyGoal ?></p>
		<p><?php echo $welcomeinfo ?></P>
</div>

<div id="info-update">

	<!--some info //-->
	<div id="update">

		<h1><?php echo $opentitle ?></h1>
		<pre><?php echo $openingtimes ?></pre>
		<pre><?php echo $address ?></pre>
		<p>&pound<?php echo $prices ?> - per pair</p>
		<p id="note"><?php echo $note ?></p>
	</div>

</div>

	<!--some sort of image box //-->

<div id="block-image">
	<img src="images/skategrind.jpeg">
	<img src="images/rink.jpg">
	<img src="images/card.jpg">
</div>