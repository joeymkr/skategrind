<?php
    $error = "";
    if (isset($_POST['submit'])){

        if (empty($_POST['phone']) || empty($_POST['addrLine1']) || empty($_POST['town']) ||empty($_POST['county']) || empty($_POST['postcode'])){
            $error = "Problem updating Database. Please try again.";
        } else {

            $connection = getConnection();

            $phone = stripslashes($_POST['phone']);
            $email = urlencode($_POST['email']);
            $facebook = urlencode($_POST['facebook']);
            $linkedin = urlencode($_POST['linkedin']);
            $twitter = urlencode($_POST['twitter']);
            $addressLine1 = stripslashes($_POST['addrLine1']);
            $addressLine2 = stripslashes($_POST['addrLine2']);
            $town = stripslashes($_POST['town']);
            $county = stripslashes($_POST['county']);
            $postcode = stripslashes($_POST['postcode']);
            $price = stripslashes($_POST['price']);

            $phone = mysqli_real_escape_string($connection, $phone);
            $email = mysqli_real_escape_string($connection, $email);
            $facebook = mysqli_real_escape_string($connection, $facebook);
            $linkedin = mysqli_real_escape_string($connection, $linkedin);
            $twitter = mysqli_real_escape_string($connection, $twitter);
            $addressLine1 = mysqli_real_escape_string($connection, $addressLine1);
            $addressLine2 = mysqli_real_escape_string($connection, $addressLine2);
            $town = mysqli_real_escape_string($connection, $town);
            $county = mysqli_real_escape_string($connection, $county);
            $postcode = mysqli_real_escape_string($connection, $postcode);
            $price = mysqli_real_escape_string($connection, $price);

            $db = mysqli_select_db($connection, "skategrind") or die("cannot select db");

            $query = mysqli_query($connection, "UPDATE info SET phone='".$phone.
                                                            "', email='".$email.
                                                            "', facebook='".$facebook.
                                                            "', linkedin='".$linkedin.
                                                            "', twitter='".$twitter.
                                                            "', address_line_1='".$addressLine1.
                                                            "', address_line_2='".$addressLine2.
                                                            "', town='".$town.
                                                            "', county='".$county.
                                                            "', postcode='".$postcode.
                                                            "', price=".floatval($price).
                                                            " WHERE phone IS NOT NULL;");
            if (!$query){
                $error = "Problem updating Database.";
                die("invalid query: ".mysqli_error($connection));
            }else {
                header("location: index.php?p=admin&control=contactInfo ");
            }
            mysqli_close($connection);
        }
    }
?>