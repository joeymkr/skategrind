<?php

    function getConnection(){
        $conn = mysqli_connect('sql05', 'skategrind', 'grinder');

        if (!$conn){
            die("cannot connect to db".$conn->mysqli_connect_error);
            return null;
        }

        return $conn;
    }

    function getOpenDays(){
            $connection = getConnection();
            $db = mysqli_select_db($connection, "skategrind");

            $query = mysqli_query($connection, "SELECT * FROM opening_times WHERE start_time IS NOT NULL AND end_time IS NOT NULL;");

            $rows;

            if($query === false){
                $rows = 0;
            } else {
                $rows = mysqli_num_rows($query);
            }
            $openingArr;

            if($rows > 0){
                while ($openingRow = mysqli_fetch_assoc($query)){
                    $openingArr[] = array("day" => $openingRow['day'], "start" => $openingRow['start_time'], "end" => $openingRow['end_time']);
                }
                mysqli_close($connection);

                return $openingArr;
            }
            mysqli_close($connection);
            return null;
        }
?>