<?php
    $error = "";
    if(isset($_POST['submit'])){

        //check if there is image to upload
        if (is_uploaded_file($_FILES['imageupload']['tmp_name'])){
            $target_dir = "images/news/";
            $target_file = $target_dir.basename($_FILES["imageupload"]["name"]);
            $uploadOk = 1;

            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

            //check if image is fake
            $check = getimagesize($_FILES['imageupload']['tmp_name']);

            if ($check == false) {
                $error = "File is not an image.";
                $uploadOk = 0;
            }

            if (empty($_POST['posttitle']) || empty($_POST['postbody'])){
                $error = "Title and Post can not be empty.";
                $uploadOk = 0;
            }

            //check if file exists
            if (file_exists($target_file)){
                $error = "Sorry, image already exists choose a different name for your image.";
                $uploadOk = 0;
            }

            //check file size
            if ($_FILES['imageupload']['size'] > 500000){
                $error = "Sorry, your file is too large.";
                $uploadOk = 0;
            }

            //allow certain file formats
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif"){
                $error = "Sorry, only JPG, JPEG, PNG & GIF are allowed.";
                $uploadOk = 0;
            }

            //check if $uploadOk is set to 0 by error
            if ($uploadOk == 0){
                $error .= "Your file was not uploaded.";
            } else {

                if(move_uploaded_file($_FILES['imageupload']['tmp_name'],$target_file)){

                    addToDb();
                } else {
                    $error .= " Error uploading file.";
                }
            }
        }
    }

    function addToDb(){
        $connection = getConnection();

        $title = stripslashes($_POST['posttitle']);
        $post = stripslashes($_POST['postbody']);
        $image = stripslashes(basename($_FILES["imageupload"]["name"]));

        $title = mysqli_real_escape_string($connection, $title);
        $post = mysqli_real_escape_string($connection, $post);
        $image = mysqli_real_escape_string($connection, $image);

        $db = mysqli_select_db($connection, "skategrind") or die("cannot select db");

        $query = mysqli_query($connection, "INSERT INTO news (title, paragraph,image)".
                                                   "VALUES ('".$title."',".
                                                            "'".$post."',".
                                                            "'".$image."');");
        if (!$query){
            $error = "Problem updating Database.";
            die("invalid query: ".mysqli_error($connection));
        }else {
            header("location: index.php?p=news");
        }
        mysqli_close($connection);
    }
?>