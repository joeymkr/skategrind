<div id="panel1">
<?php
	$pages_dir = 'includes/pages';
	if (!empty($_GET['p'])){

		$pages = scandir($pages_dir, 0);

		unset($pages[0], $pages[1]);

		$p = $_GET['p'];

		if (in_array($p.'.php', $pages)){
            if(isset($_GET['control']) && isset($_SESSION['login_user'])){

		        $pages_dir = 'includes/pages/control';
		        $pages = scandir($pages_dir, 0);
                unset($pages[0], $pages[1]);

                $control = $_GET['control'];


                if (in_array($control.'.php', $pages)){
                    include($pages_dir.'/'.$control.'.php');
                } else {
                    echo 'sorry, page not found.';
                }

		    } else {
			    include($pages_dir.'/'.$p.'.php');
			}

		} else {
		    echo 'sorry, page not found.';
		}


	} else {
		include($pages_dir.'/home.php');
	}
?>

</div>

<div id="panel2">
	<h1><span>"</span> I won't quit <span>skating</span> until I am physically unable! <span>"</span></h1>
</div>
