<?php require_once('variables/variables.php'); ?>
<div id="headContainer">

	<div id="header">
	    <?php
	    session_start();
        if (isset($_SESSION['login_user'])){
            echo '<ul id="logout"><li><a href="includes/admin/logout.php">Log out</a></li></ul><ul id="adminMenu">'.
                '<li><a href="index.php?p=news">News</a></li>'.
                '<li><a href="index.php?p=contact">Contact</a></li>'.
                '<li><a href="index.php?p=whatWeDo">About</a></li>'.
                '<li><a href="index.php">Home</a></li>'.
            '</ul>';
        } else {
            echo '<h4></h4><ul id="adminMenu" style="visibility:hidden;">'.
                '<li><a href="index.php?p=news">News</a></li>'.
                '<li><a href="index.php?p=contact">Contact</a></li>'.
                '<li><a href="index.php?p=whatWeDo">About</a></li>'.
                '<li><a href="index.php">Home</a></li>'.
            '</ul>';
        }
        ?>
        <div id="holder">
		<div><img id="logo" src="images/MrChungsPin.png"></div>

        <?php
        $p = "";
        if(isset($_GET['p'])){
            $p = $_GET['p'];
        }

        if (isset($_SESSION['login_user'])){
            echo '<div id="top-navi">'.
                '<ul>'.
                    '<li><a href="index.php?p=news">Delete Article</a></li>'.
                    '<li><a href="index.php?p=admin&control=newPost">Add Article</a></li>'.
                    '<li><a href="index.php?p=admin&control=openHours">Opening hours</a></li>'.
                    '<li><a href="index.php?p=admin&control=contactInfo">Contact info</a></li>'.
                '</ul>'.
            '</div>';
		} else {
		    echo '<div id="top-navi">'.
                '<ul>'.
                    '<li><a href="index.php?p=news">News</a></li>'.
                    '<li><a href="index.php?p=contact">Contact</a></li>'.
                    '<li><a href="index.php?p=whatWeDo">About</a></li>'.
                    '<li><a href="index.php">Home</a></li>'.
                '</ul>'.
            '</div>';
        }
		?>
		</div>
	</div>
</div>