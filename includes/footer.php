<div id="footer">

	<div id="content-info" class="column">
        <div id="column">
            <ul id="navi">
                <li><p id="footTitle">Navigation</p></li>
                <li><a href="index.php">Home</a></li>
                <li><a href="index.php?p=whatWeDo">About</a></li>
                <li><a href="index.php?p=contact">Contact</a></li>
                <li><a href="index.php?p=news">News</a></li>
             </ul>
        </div>
		<div id="column">
            <ul id="navi">
                <li><p id="footTitle">Associates</p></li>
                <li><a href="https://www.visitleevalley.org.uk/go/iceskating/">Lee Valley Ice Centre</a></li>
                <li><a href="http://www.sk8bling.co.uk/">Sk8 Bling</a></li>
                <li><a href="https://www.visitleevalley.org.uk/en/content/cms/outdoors/ice-centre/ice-skating-lessons/">Learn to Skate</a></li>
                <li><a href="https://www.visitleevalley.org.uk/en/content/cms/outdoors/ice-centre/clubs/">Ice Hockey</a></li>
            </ul>
        </div>
        <div id="column">
            <ul>
                <li><p id="connect">Social</p></li>
                <li><a <?php echo "href='".$facebookLink."'"; ?> alt="skategrind fb"><img src="images/facebook.png"></a></li>
                <li><a <?php echo "href='".$linkedinLink."'"; ?> alt="skategrind linkedin"><img src="images/linkedin.png"></a></li>
                <li><a <?php echo "href='".$twitterLink."'"; ?> alt="skategrind twitter"><img src="images/Twitter.png"></a></li>
            </ul>
		</div>
        <div id="column">
            <ul id="navi">
                <li><p id="footTitle">Contact</p></li>
                <li><pre id="footercontact"><?php echo $address ?></pre></li>
             </ul>
        </div>
    </div>
</div>