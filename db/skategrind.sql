-- phpMyAdmin SQL Dump
-- version 3.5.8.1
-- http://www.phpmyadmin.net
--
-- Host: sql05
-- Generation Time: May 13, 2015 at 03:09 PM
-- Server version: 5.6.21
-- PHP Version: 5.4.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `skategrind`
--

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE IF NOT EXISTS `info` (
  `phone` varchar(15) NOT NULL,
  `email` varchar(50) DEFAULT 'chung@skategrind.co.uk',
  `facebook` varchar(250) DEFAULT '""',
  `linkedin` varchar(250) DEFAULT '""',
  `twitter` varchar(250) DEFAULT '""',
  `address_line_1` varchar(50) NOT NULL,
  `address_line_2` varchar(50) DEFAULT '""',
  `town` varchar(50) NOT NULL,
  `county` varchar(50) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `price` decimal(25,2) DEFAULT '9.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`phone`, `email`, `facebook`, `linkedin`, `twitter`, `address_line_1`, `address_line_2`, `town`, `county`, `postcode`, `price`) VALUES
('07734667070', 'chung@skategrind.co.uk', 'http://www.facebook.com/skategrind.co.uk?fref=ts', 'http://uk.linkedin.com/pub/skate-grind/62/91a/635', NULL, 'Lee Valley Ice Centre', 'Lea Bridge Road', 'Leyton', 'London', 'E10 7QL', '9.00');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `paragraph` text,
  `image` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `date_time`, `paragraph`, `image`) VALUES
(1, 'Fashionista', '2015-05-08 17:00:20', 'Riders switching teams at the eleventh hour was nothing new. But none quite held the surprise of Eddie Lawson''s sensational switch from Yamaha to Honda, a decision that set off a sensational end-of-year clamber			in the rider market and brought Freddie Spencer out of retirement. Two of America''s finest racing exports, Lawson and Spencer had seen their fortunes differ drastically in the years following their epic ''81 AMA Superbike bow. But by the close of the decade, with six world championships between them, there was a belief they would once again face-off for the highest honours in motorcycle racing.', 'test.png');

-- --------------------------------------------------------

--
-- Table structure for table `opening_times`
--

CREATE TABLE IF NOT EXISTS `opening_times` (
  `day` enum('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday') DEFAULT NULL,
  `start_time` varchar(6) DEFAULT NULL,
  `end_time` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `opening_times`
--

INSERT INTO `opening_times` (`day`, `start_time`, `end_time`) VALUES
('Monday', NULL, NULL),
('Tuesday', NULL, NULL),
('Wednesday', NULL, NULL),
('Thursday', '18:00', '20:00'),
('Friday', NULL, NULL),
('Saturday', NULL, NULL),
('Sunday', '10:00', '12:00');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `rights` enum('ADMIN','USER') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`username`, `password`, `rights`) VALUES
('chung', 'chung2015', 'ADMIN');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
